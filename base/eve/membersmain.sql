-- phpMyAdmin SQL Dump
-- version 2.6.4-pl3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Erstellungszeit: 29. September 2006 um 17:19
-- Server Version: 4.1.21
-- PHP-Version: 4.4.4
-- 
-- Datenbank: `membertrack`
-- 

-- --------------------------------------------------------

-- 
-- Tabellenstruktur f�r Tabelle `MembersMain`
-- 

CREATE TABLE `MembersMain` (
  `MemberID` int(11) NOT NULL auto_increment,
  `LastUpdate` date default NULL,
  `Name` varchar(50) default NULL,
  `DateJoined` date default NULL,
  `RankCorp` int(11) default '0',
  `Division` int(11) default '0',
  `Deleted` tinyint(4) default '0',
  `Vacation` tinyint(4) default '0',
  `Location` varchar(50) default NULL,
  `Comment` text,
  PRIMARY KEY  (`MemberID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=MyISAM AUTO_INCREMENT=140 DEFAULT CHARSET=latin1 AUTO_INCREMENT=140 ;
