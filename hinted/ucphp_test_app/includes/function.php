<?php

function funcA()
{
    if ($_GET['A'] == 'in') {
        echo 'function A: in\n';
    }

    if ($_GET['A'] == 'out') {
        echo 'function A: out\n';
    } else {
        echo 'function A: other: '+$_GET['A']+'\n';
    }
}

function funcB($a1, $a2)
{
    $sum = $a1 + $a2;
    echo 'function B: '+$sum+'\n';
    return $sum;
}
