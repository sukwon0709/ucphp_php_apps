<?php

// Link: https://www.php.net/manual/en/faq.misc.php#faq.misc.registerglobals

// Emulate register_globals on
if (!ini_get('register_globals')) {
    $superglobals = array($_SERVER, $_ENV,
                          $_FILES, $_COOKIE, $_POST, $_GET);
    if (isset($_SESSION)) {
        array_unshift($superglobals, $_SESSION);
    }
    foreach ($superglobals as $superglobal) {
        extract($superglobal, EXTR_SKIP);
    }

    // XXX(soh): Helps ucphp to generate symbolic variables
    $action = $_REQUEST['action'];    
    $context = $_REQUEST['context'];
    $faqId = $_REQUEST['faqId'];
    $faqText = $_REQUEST['faqText'];
    $formuser = $_REQUEST['formuser'];
    $formpassword = $_REQUEST['formpassword'];    
    $helpContext = $_REQUEST['helpContext'];
    $id = $_REQUEST['id'];    
    $leftFormColor = $_REQUEST['leftFormColor'];
    $loginAttempts = $_REQUEST['loginAttempts'];    
    $message = $_REQUEST['message'];
    $newContext = $_REQUEST['newContext'];
    $newOrder = $_REQUEST['newOrder'];    
    $newParent = $_REQUEST['newParent'];    
    $newTitle = $_REQUEST['newTitle'];    
    $pageId = $_REQUEST['pageId'];
    $page_num = $_REQUEST['page_num'];
    $pageNum = $_REQUEST['pageNum'];    
    $pageTitle = $_REQUEST['pageTitle'];
    $submit = $_REQUEST['submit'];        
    $topicContext = $_REQUEST['topicContext'];    
    $topicTitle = $_REQUEST['topicTitle'];
    $topicOrder = $_REQUEST['topicOrder'];    
    $topicParent = $_REQUEST['topicParent'];
    $topicPublish = $_REQUEST['topicPublish'];
}

?>
