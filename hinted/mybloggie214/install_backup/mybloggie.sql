CREATE TABLE comment (
       comment_id int(5) NOT NULL auto_increment,
       post_id int(5) NOT NULL default '0',
       comment_subject text NOT NULL,
       com_tstamp varchar(10) NOT NULL default '',
       poster varchar(64) NOT NULL default '',
       email varchar(128) NOT NULL default '',
       home varchar(128) NOT NULL default '',
       comments text NOT NULL,
       comment_type enum('comment', 'linkback', 'trackback', 'pingback') NOT NULL default 'comment',
       ip varchar(16) NOT NULL default '',
       is_tainted tinyint(1) NOT NULL default '0',
       PRIMARY KEY (comment_id)
);

CREATE TABLE posts (
       post_id smallint(5) unsigned NOT NULL auto_increment,
       user_id varchar(4) NOT NULL default '',
       subject tinytext,
       message longtext,
       timestamp int(11) NOT NULL default '0',
       cat_id tinyint(2) NOT NULL default '0',
       allow_tb char(1) NOT NULL default '',
       is_tainted tinyint(1) NOT NULL default '0',
       PRIMARY KEY (post_id)
);

CREATE TABLE user (
       user varchar(64) NOT NULL default '',
       id tinyint(4) NOT NULL auto_increment,
       password varchar(32) NOT NULL default '',
       level tinyint(1) NOT NULL default '0',
       session_time int(1) NOT NULL default '0',
       is_tainted tinyint(1) NOT NULL default '0',
       PRIMARY KEY (id)
);

CREATE TABLE category (
       cat_id tinyint(2) NOT NULL auto_increment,
       cat_desc varchar(64) NOT NULL default '',
       is_tainted tinyint(1) NOT NULL default '0',
       PRIMARY KEY (cat_id)
);
