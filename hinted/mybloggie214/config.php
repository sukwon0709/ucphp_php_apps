<?php


// myBloggie 2.1.3 config.php file
// This file is auto generated , Please do not change anything in this file!

$dbms = 'mysql';

$dbhost = 'localhost';
$dbname = 'mybloggie';
$dbuser = 'root';
$dbpasswd = '12345';

define('INSTALLED',true );
$table_prefix = '';

$timezone = '';

define('POST_TBL',"posts" );
define('USER_TBL',"user" );
define('CAT_TBL',"category" );
define('COMMENT_TBL',"comment" );

include('setting.php');

?>