-- phpMyAdmin SQL Dump
-- version 2.6.4-pl3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Erstellungszeit: 29. September 2006 um 17:19
-- Server Version: 4.1.21
-- PHP-Version: 4.4.4
-- 
-- Datenbank: `membertrack`
-- 

-- --------------------------------------------------------

-- 
-- Tabellenstruktur für Tabelle `config`
-- 

CREATE TABLE `config` (
  `welcome` text NOT NULL,
  `ppp` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Daten für Tabelle `config`
-- 

INSERT INTO `config` VALUES ('and we are very happy to have you as our newest member. This is a little info to familiarize you with how things work in Chaos Faction! Please take some time reading it and let me know if you have any questions.\r\n\r\n Keep up to date \r\n\r\nVisit http://chaosfaction.de in game often to keep up with current events, announcements and meetings. Also, this helps us track who is active within the corp.\r\n\r\n Whom to contact? \r\n\r\nIf you are looking for advice, you have many possibilities. First, sign up on the forums (http://chaosfaction.de/forum/index.php). You may post questions there in the division subforum or you can contact the officers directly:\r\n\r\nhttp://chaosfaction.de/management.gif\r\n\r\n Inbox Settings \r\n\r\nPlease open your Evemail and right-click on the inbox tab. Select SETTINGS and select the tab ALLOWED. Please enter the following corporation there: Chaos Faction\r\n\r\nThis will help us, as then we won''t have to pay 100 ISK everytime we want to convo you or invite you to a gang. \r\n\r\nMore questions mate? Message me or post them on the \r\nforums. Our team will answer it for sure!\r\n\r\nYours,', 50000);
