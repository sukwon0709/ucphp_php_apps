<?php

define('IN_PHPBB', true);
$phpbb_root_path = './';
include $phpbb_root_path . 'extension.inc';
include $phpbb_root_path . 'common.' . $phpEx;
include $phpbb_root_path . 'includes/bbcode.' . $phpEx;
include $phpbb_root_path . 'includes/functions_post.' . $phpEx;

$params = array('forum_id' => POST_FORUM_URL, 'topic_id' => POST_TOPIC_URL, 'post_id' => POST_POST_URL);
while (list($var, $param) = @each($params)) {
    if (!empty($_POST[$param]) || !empty($_GET[$param])) {
        $$var = (!empty($_POST[$param])) ? intval($_POST[$param]) : intval($_GET[$param]);
    } else {
        $$var = '';
    }
}



if (empty($forum_id)) {
    message_die(GENERAL_MESSAGE, $lang['Forum_not_exist']);
}

$sql = "SELECT *
FROM " . FORUMS_TABLE . "
WHERE forum_id = $forum_id";

?>
