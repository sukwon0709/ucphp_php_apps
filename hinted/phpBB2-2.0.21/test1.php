<?php

/**
 * this test checks if the SELECT query at line 57 is successful.
 */

define('IN_PHPBB', true);
$phpbb_root_path = './';
include $phpbb_root_path . 'extension.inc';
include $phpbb_root_path . 'common.' . $phpEx;

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_GROUPCP);
init_userprefs($userdata);
//
// End session management
//

$script_name = preg_replace('/^\/?(.*?)\/?$/', "\\1", trim($board_config['script_path']));
$script_name = ($script_name != '') ? $script_name . '/groupcp.' . $phpEx : 'groupcp.' . $phpEx;
$server_name = trim($board_config['server_name']);
$server_protocol = ($board_config['cookie_secure']) ? 'https://' : 'http://';
$server_port = ($board_config['server_port'] != 80) ? ':' . trim($board_config['server_port']) . '/' : '/';

$server_url = $server_protocol . $server_name . $server_port . $script_name;

if (isset($_GET[POST_GROUPS_URL]) || isset($_POST[POST_GROUPS_URL])) {
    $group_id = (isset($_POST[POST_GROUPS_URL])) ? intval($_POST[POST_GROUPS_URL]) : intval($_GET[POST_GROUPS_URL]);
} else {
    $group_id = '';
}

if (isset($_POST['mode']) || isset($_GET['mode'])) {
    $mode = (isset($_POST['mode'])) ? $_POST['mode'] : $_GET['mode'];
    $mode = htmlspecialchars($mode);
} else {
    $mode = '';
}

$confirm = (isset($_POST['confirm'])) ? true : 0;
$cancel = (isset($_POST['cancel'])) ? true : 0;

$start = (isset($_GET['start'])) ? intval($_GET['start']) : 0;

//
// Default var values
//
$is_moderator = false;

if ($group_id) {

    //
    // Get group details
    //
    $sql = "SELECT *
		FROM " . GROUPS_TABLE . "
		WHERE group_id = $group_id
			AND group_single_user = 0";
    if (!($result = $db->sql_query($sql))) {
        message_die(GENERAL_ERROR, 'Error getting group information', '', __LINE__, __FILE__, $sql);
    }

    if (!($group_info = $db->sql_fetchrow($result))) {
        message_die(GENERAL_MESSAGE, $lang['Group_not_exist']);
    }

    echo "SUCCESS!\n";

}


?>