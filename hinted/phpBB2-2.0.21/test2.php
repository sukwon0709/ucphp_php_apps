<?php

define('IN_PHPBB', true);
$phpbb_root_path = './';
include $phpbb_root_path . 'extension.inc';
include $phpbb_root_path . 'common.' . $phpEx;
include $phpbb_root_path . 'includes/bbcode.' . $phpEx;
include $phpbb_root_path . 'includes/functions_post.' . $phpEx;

uc_hint_not_empty($_POST['post']);
uc_hint_not_empty($_POST['preview']);
uc_hint_not_empty($_POST['delete']);
uc_hint_not_empty($_POST['poll_delete']);
uc_hint_not_empty($_POST['add_poll_option']);
uc_hint_not_empty($_POST['edit_poll_option']);
uc_hint_not_empty($_POST['mode']);

uc_hint_not_empty($_POST[POST_FORUM_URL]);
uc_hint_not_empty($_POST[POST_TOPIC_URL]);
uc_hint_not_empty($_POST[POST_POST_URL]);

//
// Check and set various parameters
//
$params = array('submit' => 'post', 'preview' => 'preview', 'delete' => 'delete', 'poll_delete' => 'poll_delete', 'poll_add' => 'add_poll_option', 'poll_edit' => 'edit_poll_option', 'mode' => 'mode');
while (list($var, $param) = @each($params)) {
    if (!empty($_POST[$param]) || !empty($_GET[$param])) {
        $$var = (!empty($_POST[$param])) ? htmlspecialchars($_POST[$param]) : htmlspecialchars($_GET[$param]);
    } else {
        $$var = '';
    }
}

$confirm = isset($_POST['confirm']) ? true : false;

$params = array('forum_id' => POST_FORUM_URL, 'topic_id' => POST_TOPIC_URL, 'post_id' => POST_POST_URL);
while (list($var, $param) = @each($params)) {
    if (!empty($_POST[$param]) || !empty($_GET[$param])) {
        $$var = (!empty($_POST[$param])) ? intval($_POST[$param]) : intval($_GET[$param]);
    } else {
        $$var = '';
    }
}

//
// Here we do various lookups to find topic_id, forum_id, post_id etc.
// Doing it here prevents spoofing (eg. faking forum_id, topic_id or post_id
//
$error_msg = '';
$post_data = array();
switch ($mode) {
    case 'newtopic':
        if (empty($forum_id)) {
            message_die(GENERAL_MESSAGE, $lang['Forum_not_exist']);
        }

        $sql = "SELECT *
			FROM " . FORUMS_TABLE . "
			WHERE forum_id = $forum_id";
        break;

    case 'reply':
    case 'vote':
        if (empty($topic_id)) {
            message_die(GENERAL_MESSAGE, $lang['No_topic_id']);
        }

        $sql = "SELECT f.*, t.topic_status, t.topic_title, t.topic_type
			FROM " . FORUMS_TABLE . " f, " . TOPICS_TABLE . " t
			WHERE t.topic_id = $topic_id
				AND f.forum_id = t.forum_id";
        break;

    case 'quote':
    case 'editpost':
    case 'delete':
    case 'poll_delete':
        if (empty($post_id)) {
            message_die(GENERAL_MESSAGE, $lang['No_post_id']);
        }

        $select_sql = (!$submit) ? ', t.topic_title, p.enable_bbcode, p.enable_html, p.enable_smilies, p.enable_sig, p.post_username, pt.post_subject, pt.post_text, pt.bbcode_uid, u.username, u.user_id, u.user_sig, u.user_sig_bbcode_uid' : '';
        $from_sql = (!$submit) ? ", " . POSTS_TEXT_TABLE . " pt, " . USERS_TABLE . " u" : '';
        $where_sql = (!$submit) ? "AND pt.post_id = p.post_id AND u.user_id = p.poster_id" : '';

        $sql = "SELECT f.*, t.topic_id, t.topic_status, t.topic_type, t.topic_first_post_id, t.topic_last_post_id, t.topic_vote, p.post_id, p.poster_id" . $select_sql . "
			FROM " . POSTS_TABLE . " p, " . TOPICS_TABLE . " t, " . FORUMS_TABLE . " f" . $from_sql . "
			WHERE p.post_id = $post_id
				AND t.topic_id = p.topic_id
				AND f.forum_id = p.forum_id
				$where_sql";
        break;

    default:
        message_die(GENERAL_MESSAGE, $lang['No_valid_mode']);
}

?>