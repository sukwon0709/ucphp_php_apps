CREATE TABLE audit (
  modified_by_ip varchar(39) NOT NULL default '',
  modified_by_user varchar(50) NOT NULL default '',
  modified_when bigint(14) NOT NULL, 
  modified_from bigint(14) NOT NULL,
  modified_to bigint(14) NOT NULL, 
  modified_why varchar(250) NOT NULL default '',
  user_modified varchar(50) NOT NULL default '',
  PRIMARY KEY  (modified_when), 
  UNIQUE KEY modified_when (modified_when)
) TYPE=MyISAM;

CREATE TABLE dbversion (
  dbversion decimal(5,1) NOT NULL default '0.0',
  PRIMARY KEY  (dbversion)
) TYPE=MyISAM;

CREATE TABLE employees (
  empfullname varchar(50) NOT NULL default '',
  tstamp bigint(14) default NULL,
  employee_passwd varchar(25) NOT NULL default '',
  displayname varchar(50) NOT NULL default '',
  email varchar(75) NOT NULL default '',
  groups varchar(50) NOT NULL default '',
  office varchar(50) NOT NULL default '',
  admin tinyint(1) NOT NULL default '0',
  reports tinyint(1) NOT NULL default '0',
  time_admin tinyint(1) NOT NULL default '0',
  disabled tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (empfullname)
) TYPE=MyISAM;

CREATE TABLE groups (
  groupname varchar(50) NOT NULL default '',
  groupid int(10) NOT NULL auto_increment,
  officeid int(10) NOT NULL default '0',
  PRIMARY KEY  (groupid)
) TYPE=MyISAM;

CREATE TABLE info (
  fullname varchar(50) NOT NULL default '',
  `inout` varchar(50) NOT NULL default '',
  timestamp bigint(14) default NULL,
  notes varchar(250) default NULL,
  ipaddress varchar(39) NOT NULL default '',
  KEY fullname (fullname)
) TYPE=MyISAM;

CREATE TABLE metars (
  metar varchar(255) NOT NULL default '',
  timestamp timestamp(14) NOT NULL,
  station varchar(4) NOT NULL default '',
  PRIMARY KEY  (station),
  UNIQUE KEY station (station)
) TYPE=MyISAM;

CREATE TABLE offices (
  officename varchar(50) NOT NULL default '',
  officeid int(10) NOT NULL auto_increment,
  PRIMARY KEY  (officeid)
) TYPE=MyISAM;

CREATE TABLE punchlist (
  punchitems varchar(50) NOT NULL default '',
  color varchar(7) NOT NULL default '',
  in_or_out tinyint(1) default NULL,
  PRIMARY KEY  (punchitems)
) TYPE=MyISAM;
